//
//  AddViewController.swift
//  MyPassword
//
//  Created by Un Vandy on 1/20/19.
//  Copyright © 2019 Un Vandy. All rights reserved.
//

import UIKit
import TextFieldEffects
import CoreData

class AddViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var titleTextField: TextFieldEffects!
    @IBOutlet weak var usernameTextField: AkiraTextField!
    @IBOutlet weak var passwordTextField: AkiraTextField!
    @IBOutlet weak var confirmPasswordTextField: AkiraTextField!
    @IBOutlet weak var categoryTextField: UITextField!
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var passwords: Password?
    var result: String?
    let dataSource = ["Facebook","Apple","Google","Gmail","Phone","Credit Card","Computer","Other"]
    let picker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        categoryTextField.inputView = picker
        //categoryPickerView()
        selfDelegate()
       
        if let cont = passwords {
            self.navigationItem.title = "Edit Password"
            self.titleTextField.text = cont.title
            self.usernameTextField.text = cont.username
            self.passwordTextField.text = cont.password
        }
        
    }
    
    @IBAction func saveButton(_ sender: Any) {
        result = checkEmpty()
        if (!(result!.isEmpty)){
            showAlert(title: "Warning", message: result!)
        }else{
            if self.passwords == nil{ // save
                let password = Password(context: context)
                password.title = titleTextField.text
                password.username = usernameTextField.text
                password.password = passwordTextField.text
                password.status = true
                appDelegate.saveContext()
                clearText()
                showAlert(title: "Success", message: "Save successfully")
                //performSegue(withIdentifier: "saveToHomeSegue", sender: nil)
            }else { // update
                let updatePassword = findPassword(id: (self.passwords?.objectID)!)
                updatePassword?.title = titleTextField.text
                updatePassword?.username = usernameTextField.text
                updatePassword?.password = passwordTextField.text
                appDelegate.saveContext()
                clearText()
                showAlert(title: "Success", message: "Update successfully")
            }
        }
    }
    
    func findPassword(id:NSManagedObjectID) -> Password? {
        if let passwords = context.object(with: id) as? Password{
            return passwords
        }
        return nil
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ( segue.identifier == "addSegue"){
            let dest = segue.destination as? AddViewController
            dest?.passwords = (sender as! Password)
        }
    }
    
    //Create Category PickerView
    func categoryPickerView() {
        let categoryPicker = UIPickerView()
        categoryPicker.delegate = self
        categoryTextField.inputView = categoryPicker
       
        categoryPicker.backgroundColor = .white
        
    }
    
    //Create Done Button
    func createToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(AddViewController.dismissKeyboard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        categoryTextField.inputAccessoryView = toolBar
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(false)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == titleTextField {
            self.usernameTextField.becomeFirstResponder()
        }else if textField == usernameTextField {
            self.passwordTextField.becomeFirstResponder()
        }else if textField == passwordTextField {
            self.confirmPasswordTextField.becomeFirstResponder()
        }else if textField == passwordTextField {
            self.categoryTextField.becomeFirstResponder()
        }else if textField == categoryTextField {
            return true
        }
        return false
    }
    func checkEmpty() -> String {
        if (titleTextField.text!.isEmpty){
            result = "Please input Title !"
        }else if (usernameTextField.text!.isEmpty){
            result = "Please input Username !"
        }else if (passwordTextField.text!.isEmpty){
            result = "Please input Password !"
        }else if (confirmPasswordTextField.text! != passwordTextField.text!){
            result = "Confirm password is not match !"
        }else if (categoryTextField.text!.isEmpty){
            result = ""
        }
        return result!
    }
    func clearText() {
        titleTextField.text = ""
        usernameTextField.text = ""
        passwordTextField.text = ""
        confirmPasswordTextField.text = ""
        categoryTextField.text = ""
        //titleTextField.becomeFirstResponder()
    }
    func selfDelegate() {
        self.titleTextField.delegate = self
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
        self.confirmPasswordTextField.delegate = self
        self.categoryTextField.delegate = self
    }
    public func showAlert(title: String,message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

}

extension AddViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        categoryTextField.text = dataSource[row]
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource[row]
    }

}


