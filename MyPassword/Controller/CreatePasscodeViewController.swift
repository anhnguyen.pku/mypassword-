//
//  PasscodeViewController.swift
//  MyPassword
//
//  Created by Un Vandy on 2/10/19.
//  Copyright © 2019 Un Vandy. All rights reserved.
//

import UIKit

class CreatePasscodeViewController: UIViewController,UITextFieldDelegate {
    //New passcode
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    //Confirm passcode
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        newPasswordTextField.delegate = self
        confirmPasswordTextField.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        newPasswordTextField.becomeFirstResponder()
    }
    @objc func dismissKeyboard() {
        view.endEditing(false)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func createButton(_ sender: Any) {
        let newPass = newPasswordTextField.text!
        let confirmPass = confirmPasswordTextField.text!
        if newPass != confirmPass || newPass == "" { // Password not match
            showAlert(title: "Incorrect", message: "Confirm Password not match !")
        }else { // Password match and save to userdefault
               //performSegue(withIdentifier: "newPasscodeToHome", sender: nil)
            UserDefaults.standard.set(confirmPass, forKey: "Password1")
         
        }
    
    }
    
    public func showAlert(title: String,message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
